<?php
//like payments
class Iblockelementpropertyar extends ActiveRecord\Model
{
	static $table_name = 'b_iblock_element_property';
	// payment belongs to a person
	static $belongs_to = array(
		array('iblockelementar'),
		array('iblockpropertyar', 'primary_key' => 'iblock_property_id', 'foreign_key' => 'id')
		);
	static $has_many = array(		
		array('iblockpropertyar', 'primary_key' => 'iblock_property_id', 'foreign_key' => 'id')
		);	
};
?>