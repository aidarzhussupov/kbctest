<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тестовая страница ActiveRecord");
?>
<?	
	class IblockElementKBC {
	
		
		//метод инициализирует PHP ActiveRecord
		function initializeAR(){
			// подключаем необходимые файлы для PHPActiveRecord
			require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/php-activerecord/ActiveRecord.php';	
			
			//устанавливаем соединение с БД и указываем папку с нашими моделями для работы с таблицами
			ActiveRecord\Config::initialize(function($cfg)
			{
				$cfg->set_model_directory($_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/php-activerecord/mymodels');
				$cfg->set_connections(array('development' =>'mysql://root:@localhost/kbctest;charset=utf8'));
			});		
		}
		
		//метод возвращает элемент инфоблока по заданному ID элемента и ID инфоблока
		function getElement($iblockID, $elementID){
			//получаем сам элемент
			$obElement = Iblockelementar::find(array('conditions' => array('iblock_id = ? AND id = ?', $iblockID, $elementID)));
			if(is_object($obElement)){
				return $obElement;
			} else {
				return false;
			}
			
		}
		//Метод возвращает все значения свойств элемента
		function getElementProperties($iblockID, $elementID){
			//массив со значениями свойств элемента
			$arElementProps = array();
			//получаем сам элемент
			$arElementPropsValues = Iblockelementpropertyar::all(array('conditions' => array('iblock_element_id = ?',$elementID )));
			
			foreach ($arElementPropsValues as $key => $propValue){		
				//получаем параметры соответствующего свойства
				$arElementPropDescr = Iblockpropertyar::find($propValue->iblock_property_id);
				//заполняем массив значений свойств элемента
				$arElementProps[$key]['iblock_property_id'] = $propValue->iblock_property_id;
				$arElementProps[$key]['value'] = $propValue->iblock_property_id;
				$arElementProps[$key]['prop_name'] = $arElementPropDescr->name;
				$arElementProps[$key]['prop_code'] = $arElementPropDescr->code;			
			
			}
			
			if(is_array($arElementProps)){
				return $arElementProps;
			} else {
				return false;
			}
			
		}
		//получаем всю информацию о элемента включая название элемента и значения свойств
		function getElementInfo($iblockID, $elementID){
			//массив с информацией   элемента и его свойств
			$arElementInfo = array();
			//получаем сам элемент
			$obElement = Iblockelementar::find(array('conditions' => array('iblock_id = ? AND id = ?', $iblockID, $elementID)));
			if(is_object($obElement)){
				$arElementPropsValues = Iblockelementpropertyar::all(array('conditions' => array('iblock_element_id = ?',$elementID )));
				
				$arElementProps['ELEMENT_ID'] = $obElement->id;
				$arElementProps['NAME'] = $obElement->name;
				$arElementProps['MODIFIED_BY'] = $obElement->modified_by;
				$arElementProps['CREATED_BY'] = $obElement->created_by;
				$arElementProps['PREVIEW_PICTURE'] = $obElement->preview_picture;
				$arElementProps['PREVIEW_TEXT'] = $obElement->preview_text;
				$arElementProps['DETAIL_PICTURE'] = $obElement->detail_picture;
				$arElementProps['DETAIL_TEXT'] = $obElement->detail_text;
				$arElementProps['DATE_CREATE'] = $obElement->date_create;
				
				foreach ($arElementPropsValues as $key => $propValue){		
					//получаем параметры соответствующего свойства
					$arElementPropDescr = Iblockpropertyar::find($propValue->iblock_property_id);
					//заполняем массив значений свойств элемента				
					
					$arElementProps['PROPERTY'][$key]['IBLOCK_PROPERTY_ID'] = $propValue->iblock_property_id;
					$arElementProps['PROPERTY'][$key]['VALUE'] = $propValue->value;
					$arElementProps['PROPERTY'][$key]['PROP_NAME'] = $arElementPropDescr->name;
					$arElementProps['PROPERTY'][$key]['PROP_CODE'] = $arElementPropDescr->code;			
				
				}
				if(is_array($arElementProps)){
					return $arElementProps;
				} else {
					return false;
				}
			}
		}
		
		//TODO: придумать механизм изменения множественных свойств
		//метод обновляет информацию о элементе
		//$arFields('fieldName' => 'value')		
		//не может менять множественные свйоства =(
		function updateElement($iblockID, $elementID, $arFields, $arProps){		
				
			 $arPropAliases = array(
				'VIEWS' => 1,
				'ATTACHED_FILES' => 2
			);
	
			if($elementID > 0 && $iblockID > 0){
				$obElement = Iblockelementar::find(array('conditions' => array('iblock_id = ? AND id = ?', $iblockID, $elementID)));
				if(is_array($arFields)){
					foreach($arFields as $propertyID => $fieldValue){
						$obElement->$propertyID = $fieldValue;
					}
				}
				$obElement->save();				
				
				foreach ($arProps as $propCode => $propValue){
					$arElementProps = Iblockelementpropertyar::find(array('conditions' => array('iblock_element_id = ? AND iblock_property_id = ?',$elementID, $arPropAliases[$propCode] )));					
					$arElementProps->value = $propValue;
					$arElementProps->save();
				}
								
				
				
				if (count($obElement->errors) > 0 && count($arElementProps->errors) > 0){
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}
		
		//метод удаляет элемент и значения его свойств тоже
		function deleteElement($iblockID, $elementID){
			$obElement = Iblockelementar::find(array('conditions' => array('iblock_id = ? AND id = ?', $iblockID, $elementID)));
			$obElement->delete();
			
			$arElementPropsValues = Iblockelementpropertyar::all(array('conditions' => array('iblock_element_id = ?',$elementID )));
			foreach($arElementPropsValues as $arElementPropsValue ){
				$arElementPropsValue->delete();
			}
			if(count($obElement->errors) > 0 && count($arElementPropsValues->errors) > 0){
				return false;
			} else {
				return true;
			}
			
		}
};	
	
	
		
	//working iblock ID
	$iblockID = 1;
	
	//working element ID		
	$elementID = 2;	
	
	
		
	//initialize AR
	IblockElementKBC::initializeAR();	
	
	//получаем сам элемент
	$obElement = IblockElementKBC::getElement($iblockID, $elementID);
	
	//получаем значения свойств элемента
	$arElementPropValue = IblockElementKBC::getElementProperties($iblockID, $elementID);
	
	//получаем информацию о элементе влючая значения всех свойств
	$arElementInfo = IblockElementKBC::getElementInfo($iblockID, $elementID);	
	
	IblockElementKBC::updateElement($iblockID, $elementID,array('name' => 'Новое имя 2', 'modified_by' => 3), array('VIEWS' => '777'));
	
	//IblockElementKBC::deleteElement($iblockID, $elementID);
	
	echo '<pre>';
	print_r(IblockElementKBC::getElementInfo($iblockID, $elementID));
	echo '</pre>';
	
		
	
	echo "$obElement->name - значения свойств : " . join(', ',ActiveRecord\collect($obElement->iblockelementpropertyar,'value')) . "\n";
	
	
	
	
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>